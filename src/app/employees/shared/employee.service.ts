import { Injectable } from '@angular/core';
import{Http,Response,Headers,RequestOptions,RequestMethod} from '@angular/http';
//import 'rxjs/add/operator/map';
//import 'rxjs/Rx';
//import 'rxjs/add/operator/toPromise';

import{Employee} from'./employee.model'
@Injectable()
export class EmployeeService {

  selectedEmployee:Employee;
  constructor( private http:Http) { 
  }
  postEmployee(emp :Employee){
    var body = JSON.stringify(emp);
    var headerOptions=new Headers({'Content-type':'application/json'});
    var requestoprions=new RequestOptions({method:RequestMethod.Post,headers:headerOptions})
    console.log(body);
    return this.http.post('	http://dummy.restapiexample.com/api/v1/create', body,requestoprions)
  }
}
